# TODO

The plan is to make a blogging platform. As such the server should support:

1. HTML templating
2. Static serving of css
3. RSS feed
4. Atom feed
5. Option to serve JS if necessary
6. Potentially (eventually) moving to a full stack rust setup
