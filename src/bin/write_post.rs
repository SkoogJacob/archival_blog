use archival_blog::{db::queries::create_post, *};
use std::io::{stdin, Read};

fn main() {
    let connection = &mut establish_connection();

    let mut title = String::new();
    let mut body = String::new();

    println!("What would you like your title to be?");
    stdin().read_line(&mut title).unwrap();
    let title = title.trim();

    println!(
        "\nOk! Let's write {} (Press {} when finished)\n",
        title, "CTRL+D"
    );

    stdin().read_to_string(&mut body).unwrap();
    let body = body.trim();
    let post = create_post(connection, title, body);
    println!("\nSaved draft {} with id {}", title, post.id)
}
