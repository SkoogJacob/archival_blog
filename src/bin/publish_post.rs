use archival_blog::{db::models::Post, establish_connection};
use diesel::prelude::*;
use std::env::args;

fn main() {
    use archival_blog::schema::posts::dsl::{posts, published};

    let id = args()
        .nth(1)
        .expect("publish_post requires a post id")
        .parse::<i32>()
        .expect("Invalid ID. Must be parseable to an integer.");
    let conn = &mut establish_connection();
    let post = diesel::update(posts.find(id))
        .set(published.eq(true))
        .returning(Post::as_returning())
        .get_result(conn)
        .unwrap();
    println!("Published post {}", post.title)
}
