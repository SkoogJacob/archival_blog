use archival_blog::db::models::*;
use diesel::prelude::*;

#[tokio::main]
async fn main() {
    use archival_blog::schema::posts::dsl::*;
    let conn = &mut archival_blog::establish_connection();
    let results = posts
        .filter(published.eq(true))
        .limit(5)
        .select(Post::as_select())
        .load(conn)
        .expect("Error loading posts");

    println!("Displaying {} posts", results.len());

    for post in results {
        println!("\n{}", post.title);
        println!("-------------\n");
        println!("{}\n", post.body);
    }
}
